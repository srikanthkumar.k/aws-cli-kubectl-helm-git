FROM amazon/aws-cli:2.3.0
MAINTAINER srikanthkumar.k@yahoo.com
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && chmod +x ./kubectl && mv ./kubectl /usr/local/bin/kubectl && kubectl version --client
RUN yum -y -q install tar gzip git && curl -LO "https://get.helm.sh/helm-v3.7.1-linux-amd64.tar.gz" && tar -zxvf helm-v3.7.1-linux-amd64.tar.gz && mv linux-amd64/helm /usr/local/bin/helm && helm version
